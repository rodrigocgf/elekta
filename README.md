# Elekta C# code test

* to run : dotnet test

This program will generate the desired file PetsReport.csv, which will be used
by the unit test Test_3_Pets in the class MyTestClass to check if the output file
will contain 3 pets.

Another file with a preety output is also generated. It is named PetsReport.txt.
