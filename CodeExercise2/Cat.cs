


namespace CodeExcercise
{
    public class Cat : Pet
    {
        public Cat(): base()
        {
            NumberOfLives = 0;
            CostPerVisit = 0.0;
        }

        public Cat(int _NumberOfLives , double _CostPerVisit )
        {
            NumberOfLives = _NumberOfLives;
            CostPerVisit = _CostPerVisit;
        }

        public int NumberOfLives { get; set; }
        public double CostPerVisit { get; set; }

    }
}