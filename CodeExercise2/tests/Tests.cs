
namespace CodeExcercise
{
    [TestClass]
    public class MyTestClass
    {
        [TestMethod]
        public void Test_3_Pets()
        {

            PetBuilder pb1 = new PetBuilder();
            pb1.BuildDog().
                BuildOwner("Jim", "Rogers").
                BuildNumberOfVisits(5).
                BuildJoinedPractice(DateTime.Now);

            PetBuilder pb2 = new PetBuilder();
            pb2.BuildDog(19.36).
                BuildOwner("Tony", "Smith").
                BuildNumberOfVisits(10).
                BuildJoinedPractice(new DateTime(1985, 7, 13));

            PetBuilder pb3 = new PetBuilder();
            pb3.BuildCat(9, 21.93).
                BuildOwner("Steve", "Roberts").
                BuildNumberOfVisits(20).
                BuildJoinedPractice(new DateTime(2002, 5, 6)).
                BuildNumberOfLives(9);

            List<Pet> pets = new List<Pet>()
            {
                pb1.GetDog(),
                pb2.GetDog(),
                pb3.GetCat()
            };

            Reports reps = new Reports();
            reps.PrintReport(pets, "PetsReport.csv");
            reps.PrintReport_Preety(pets, "PetsReport.txt");
            string[] outPets = File.ReadAllLines("PetsReport.csv");

            Assert.AreEqual(3, outPets.Count() - 1);
        }
    }

}