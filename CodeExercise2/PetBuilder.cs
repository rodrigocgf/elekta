﻿using System;
using CodeExcercise;


namespace CodeExcercise
{
	public class PetBuilder : AbstractPetBuilder
	{

        public override PetBuilder BuildJoinedPractice(DateTime _JoinedPractice)
        {
            if ( cat != null)
                cat.JoinedPractice = _JoinedPractice;

            if (dog != null)
                dog.JoinedPractice = _JoinedPractice;

            return this;
        }

        public override PetBuilder BuildNumberOfLives(int _NumberOfLives)
        {
            if (cat != null)
                cat.NumberOfLives = _NumberOfLives;

            return this;
        }

        public override PetBuilder BuildNumberOfVisits(int _NumberOfVisits)
        {
            if ( cat != null)
                cat.NumberOfVisits = _NumberOfVisits;

            if (dog != null)
                dog.NumberOfVisits = _NumberOfVisits;

            return this;
        }

        public override PetBuilder BuildOwner(string _FirstName, string _LastName)
        {
            if ( cat != null)
            {
                cat.FirstName = _FirstName;
                cat.LastName = _LastName;
            }

            if ( dog != null )
            {
                dog.FirstName = _FirstName;
                dog.LastName = _LastName;
            }

            return this;
        }

        public override PetBuilder BuildDog(double _CostPerVisit = 0.0)
        {
            if ( dog == null)
            {
                dog = new Dog(_CostPerVisit);
            }

            return this;
        }

        public override PetBuilder BuildCat(int _NumberOfLives , double _CostPerVist)
        {
            
            if ( cat == null)
            {
                cat = new Cat(_NumberOfLives, _CostPerVist);
            }

            return this;
        }
        

        public Cat? GetCat()
        {
            return cat;
        }

        public Dog? GetDog()
        {
            return dog;
        }
    }
}

