﻿using System;
using System.Reflection;

namespace CodeExcercise
{
	public class Reports
	{
		public Reports()
		{
		}

        public const int OWNERS_LEN = 16;
        public const int DATE_JOINED_LEN = 26;
        public const int NUMBER_OF_VISITS_LEN = 26;
        public const int NUMBER_OF_LIVES = 20;
        public const int COST_PER_VISIT = 20;

        // NOTE: This method prints a pets reports in csv format.
        public void PrintReport(IEnumerable<Pet> pets, string filename)
        {
            Console.WriteLine("[Pet::printReport]");

            const string strOwners = "Owners name";
            const string strDataJoined = "Date Joined Practice";
            const string strNumVisits = "Number Of Visits";
            const string strNumLives = "Number of Lives";
            const string strCostPerVisit = "Cost per Visit";

            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string executablePath = Path.GetDirectoryName(assemblyPath);
            string filePath = Path.Combine(executablePath, filename);

            List<string> entries = new List<string>();

            string strFormat = "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"";

            Func<string, int, string> insertSpaces = (originalString, totalSize) =>
            {
                int NumLeftSpaces = 0;
                int NumRightSpaces = 0;

                if ((totalSize - originalString.Length) % 2 == 0)
                {
                    NumLeftSpaces = (totalSize - originalString.Length) / 2;
                }
                else
                {
                    NumLeftSpaces = (totalSize - originalString.Length) / 2 + 1;
                }

                string LeftSpaces = new string(' ', NumLeftSpaces);
                NumRightSpaces = totalSize - originalString.Length - NumLeftSpaces;
                string RightSpaces = new string(' ', NumRightSpaces);
                return LeftSpaces + originalString + RightSpaces;
            };

            string strEntry = String.Format(strFormat,
                                            insertSpaces(strOwners, OWNERS_LEN),
                                            insertSpaces(strDataJoined, DATE_JOINED_LEN),
                                            insertSpaces(strNumVisits, NUMBER_OF_VISITS_LEN),
                                            insertSpaces(strNumLives, NUMBER_OF_LIVES),
                                            insertSpaces(strCostPerVisit, COST_PER_VISIT));

            entries.Add(strEntry);


            foreach (Pet p in pets)
            {

                string entry;

                if (p != null)
                {
                    if (p is Cat)
                    {

                        entry = String.Format(strFormat,
                                            insertSpaces(string.Join(" ", p.FirstName, p.LastName), OWNERS_LEN),
                                            insertSpaces(p.JoinedPractice.ToString(), DATE_JOINED_LEN),
                                            insertSpaces(p.NumberOfVisits.ToString(), NUMBER_OF_VISITS_LEN),
                                            insertSpaces(((Cat)p).NumberOfLives.ToString(), NUMBER_OF_LIVES),
                                            insertSpaces(((Cat)p).CostPerVisit.ToString(), COST_PER_VISIT));
                        Console.WriteLine(entry);

                        entries.Add(entry);
                    }
                    if (p is Dog)
                    {

                        entry = String.Format(strFormat,
                                            insertSpaces(string.Join(" ", p.FirstName, p.LastName), OWNERS_LEN),
                                            insertSpaces(p.JoinedPractice.ToString(), DATE_JOINED_LEN),
                                            insertSpaces(p.NumberOfVisits.ToString(), NUMBER_OF_VISITS_LEN),
                                            insertSpaces("1", NUMBER_OF_LIVES),
                                            insertSpaces(((Dog)p).CostPerVisit.ToString(), COST_PER_VISIT));

                        Console.WriteLine(entry);

                        entries.Add(entry);
                    }
                }

            }

            File.WriteAllLines(filePath.ToString(), entries.ToArray());
        }


        public void PrintReport_Preety(IEnumerable<Pet> pets, string filename)
        {
            Console.WriteLine("[Pet::PrintReport_Preety]");

            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string executablePath = Path.GetDirectoryName(assemblyPath);
            string filePath = Path.Combine(executablePath, filename);


            List<string> entries = new List<string>();
            const string strOwners = "Owners name";
            const string strDataJoined = "Date Joined Practice";
            const string strNumVisits = "Number Of Visits";
            const string strNumLives = "Number of Lives";
            const string strCostPerVisit = "Cost per Visit";

            char character = '-';

            string tracesString;
            tracesString = "|" + String.Concat(Enumerable.Repeat(character.ToString(), OWNERS_LEN));
            tracesString += "|" + String.Concat(Enumerable.Repeat(character.ToString(), DATE_JOINED_LEN));
            tracesString += "|" + String.Concat(Enumerable.Repeat(character.ToString(), NUMBER_OF_VISITS_LEN));
            tracesString += "|" + String.Concat(Enumerable.Repeat(character.ToString(), NUMBER_OF_LIVES));
            tracesString += "|" + String.Concat(Enumerable.Repeat(character.ToString(), COST_PER_VISIT)) + "|";

            entries.Add(tracesString);

            string strFormat = "|{0}|{1}|{2}|{3}|{4}|";

            Func<string, int, string> insertSpaces = (originalString, totalSize) =>
            {
                int NumLeftSpaces = 0;
                int NumRightSpaces = 0;

                if ((totalSize - originalString.Length) % 2 == 0)
                {
                    NumLeftSpaces = (totalSize - originalString.Length) / 2;
                }
                else
                {
                    NumLeftSpaces = (totalSize - originalString.Length) / 2 + 1;
                }

                string LeftSpaces = new string(' ', NumLeftSpaces);
                NumRightSpaces = totalSize - originalString.Length - NumLeftSpaces;
                string RightSpaces = new string(' ', NumRightSpaces);
                return LeftSpaces + originalString + RightSpaces;
            };

            string strEntry = String.Format(strFormat,
                                            insertSpaces(strOwners, OWNERS_LEN),
                                            insertSpaces(strDataJoined, DATE_JOINED_LEN),
                                            insertSpaces(strNumVisits, NUMBER_OF_VISITS_LEN),
                                            insertSpaces(strNumLives, NUMBER_OF_LIVES),
                                            insertSpaces(strCostPerVisit, COST_PER_VISIT));

            entries.Add(strEntry);

            entries.Add(tracesString);


            foreach (Pet p in pets)
            {

                string entry;

                if (p != null)
                {
                    if (p is Cat)
                    {

                        entry = String.Format(strFormat,
                                            insertSpaces(string.Join(" ", p.FirstName, p.LastName), OWNERS_LEN),
                                            insertSpaces(p.JoinedPractice.ToString(), DATE_JOINED_LEN),
                                            insertSpaces(p.NumberOfVisits.ToString(), NUMBER_OF_VISITS_LEN),
                                            insertSpaces(((Cat)p).NumberOfLives.ToString(), NUMBER_OF_LIVES),
                                            insertSpaces(((Cat)p).CostPerVisit.ToString(), COST_PER_VISIT));

                        Console.WriteLine(entry);

                        entries.Add(entry);
                    }
                    if (p is Dog)
                    {

                        entry = String.Format(strFormat,
                                            insertSpaces(string.Join(" ", p.FirstName, p.LastName), OWNERS_LEN),
                                            insertSpaces(p.JoinedPractice.ToString(), DATE_JOINED_LEN),
                                            insertSpaces(p.NumberOfVisits.ToString(), NUMBER_OF_VISITS_LEN),
                                            insertSpaces("1", NUMBER_OF_LIVES),
                                            insertSpaces(((Dog)p).CostPerVisit.ToString(), COST_PER_VISIT));

                        Console.WriteLine(entry);

                        entries.Add(entry);
                    }
                }

            }

            entries.Add(tracesString);

            File.WriteAllLines(filePath.ToString(), entries.ToArray());
        }
    }
}

