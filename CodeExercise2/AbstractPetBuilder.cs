﻿using System;
using CodeExcercise;

namespace CodeExcercise
{
	abstract public class AbstractPetBuilder
	{
        protected Cat? cat;
		protected Dog? dog;

		public AbstractPetBuilder()
		{
			cat = null;
			dog = null;
		}

		public abstract AbstractPetBuilder BuildCat(int _NumberOfLives, double CostPerVist);
        public abstract AbstractPetBuilder BuildDog(double CostPerVist);

        public abstract AbstractPetBuilder BuildNumberOfLives(int _NumberOfLives);
        public abstract AbstractPetBuilder BuildNumberOfVisits(int _NumberOfVisits);
		public abstract AbstractPetBuilder BuildJoinedPractice(DateTime _JoinedPractice);
		public abstract AbstractPetBuilder BuildOwner(string _FirstName, string _LastName);


    }
}

