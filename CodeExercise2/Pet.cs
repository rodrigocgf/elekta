
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CodeExcercise
{
    public class Pet : Owner
    {

        public int NumberOfVisits { get; set; }
        public DateTime JoinedPractice { get; set; }

        public Pet()
        { }

        public Pet( int _NumberOfVisits, DateTime _JoinedPractice)
        {
            NumberOfVisits = _NumberOfVisits;
            JoinedPractice = _JoinedPractice;
        }
    }


}