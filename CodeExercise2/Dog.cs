

namespace CodeExcercise
{
    public class Dog : Pet
    {
        public Dog() : base()
        {
            CostPerVisit = 0.0;
        }

        public Dog( double _CostPerVisit)
        {
            CostPerVisit = _CostPerVisit;
        }

        public double CostPerVisit { get; set; }

    }
}