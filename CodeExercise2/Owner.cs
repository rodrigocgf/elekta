
namespace CodeExcercise
{
    public class Owner
    {
        public Owner()
        { }

        public Owner(string _FirstName, string _LastName)
        {
            FirstName = _FirstName;
            LastName = _LastName;
        }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

    }
}